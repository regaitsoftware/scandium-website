Scandium-Website
----------------

This repo contains all the code to run the Scandium Website on an IIS instance.

* Supports Login/Registration for practitioners and patients
* Supports OAuth for practitioners and patients
* Supports graphical representation of patient data for practitioner homepage

How to Run
-----------

Upon initial run, visual studio should create a mssql database, but before
running first you must seed the database.

1. Go to Tools -> Nuget Package Manager -> Package Manager Console
2. This should prompt you to restore some packages, do this.
3. Enter the command "update-database"
   This will seed the database with a test patient and practitioner so you do not
   need to register those users. The practitioner will be assigned that test patient.
   If you want to create your own users, you must make sure the login call in the scandium
   repo points to the right username, currently the username is hardcoded for now.
4. Go to server explorer and double check you have two users under the user tables, two roles,
   two user roles, and four hospitals.
5. Build and run


Notes
------
* This should be setup before running the regular scandium wpf application.

* Patients and Practitioners go through the same login form so enter either "testpatient@example.com"
  or "testpractitioner@example.com" with a pin 000000 and you should be able to login.

* Currently the only interesting part of the website is for the practitioner homepage where a patient's
  session data can be graphed.

* Make sure the site is serving files of HTTPS otherwise OAuth will fail.

* PINs must be 6 digits long. If you get an error message on registration it is probably due to incorrect pin lengths.

* Emails must be unique.

* NPI's are not unique.

* To view other constraints look at the table designer in server explorer to see which entries are required.
