using System;
using System.Collections.Generic;

namespace ScandiumWebsite.Models
{
    public class Session : IdentifiableModel
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public virtual Patient Patient { get; set; }
        public virtual ICollection<Datapoint> Datapoints { get; set; }
    }
}
