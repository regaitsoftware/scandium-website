﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ScandiumWebsite.Models
{
    public class ExternalLoginViewModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }
    }

    public class UserLoginInfoViewModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
    }

    public class UserInfoViewModel
    {
        public string Email { get; set; }

        public bool HasRegistered { get; set; }

        public string LoginProvider { get; set; }
    }

    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string Email { get; set; }

        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    }

    public class PractitionerRegisterViewModel : AuthenticatablePersonViewModel
    {
        [Required]
        [Display(Name = "NPI")]
        [RegularExpression("^[0-9]{10}$", ErrorMessage = "NPI must be a 10 digit long number.")]
        public string NPI { get; set; }

        [Required]
        [Display(Name = "Hospital")]
        public string HospitalId { get; set; }

        [Display(Name = "Patients")]
        public IEnumerable<string> PatientIds { get; set; }
    }

    public class PatientRegisterViewModel : AuthenticatablePersonViewModel
    {
        public int SelectHospitalId { get; set; }
        [Required]
        [Display(Name = "Hospital")]
        public string HospitalId { get; set; }

        [Display(Name = "Practitioner")]
        public string PractitionerId { get; set; }
    }

    public class PersonViewModel
    {
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
    }

    public class AuthenticatablePersonViewModel : PersonViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [RegularExpression("^[0-9]{6}$", ErrorMessage = "The PIN must be 6 digits long")]
        [Display(Name = "PIN")]
        public string PIN { get; set; }

        [Display(Name = "Confirm PIN")]
        [System.ComponentModel.DataAnnotations.Compare("PIN", ErrorMessage = "The PIN and confirmation PIN do not match.")]
        public string ConfirmPIN { get; set; }
    }

    public class SetPINBindingModel
    {
        [Required]
        [RegularExpression("^[0-9]{6}$", ErrorMessage = "The PIN must be 6 digits long")]
        [DataType(DataType.Password)]
        [Display(Name = "New PIN")]
        public string NewPIN { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new PIN")]
        [System.ComponentModel.DataAnnotations.Compare("NewPIN", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPIN { get; set; }
    }

    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }

        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "PIN")]
        public string PIN { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class ResetPINViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [RegularExpression("^[0-9]{6}$", ErrorMessage = "The PIN must be 6 digits long")]
        [DataType(DataType.Password)]
        [Display(Name = "PIN")]
        public string PIN { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm PIN")]
        [System.ComponentModel.DataAnnotations.Compare("PIN",
            ErrorMessage = "The PIN and confirmation PIN do not match.")]
        public string ConfirmPIN { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class RequestPatientChartViewModel
    {
        [Required]
        [Display(Name = "Patient Id")]
        public string PatientId { get; set; }

        [Display(Name = "Session Id")]
        public string SessionId { get; set; }

        [Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }

        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }
    }
}
