﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using ScandiumWebsite.Interfaces;

namespace ScandiumWebsite.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser, IPerson
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        public virtual Patient Patient { get; set; }
        public virtual Practitioner Practitioner { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, string> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }


    public class RegaitContext : IdentityDbContext<ApplicationUser>, IRegaitContext
    {
        public RegaitContext() : base("name=RegaitContext", throwIfV1Schema: false)
        {
            RequireUniqueEmail = true;
        }

        public virtual DbSet<Hospital> Hospitals { get; set; }
        public virtual DbSet<Session> Sessions { get; set; }
        public virtual DbSet<Practitioner> Practitioners { get; set; }
        public virtual DbSet<Patient> Patients { get; set; }
        public virtual DbSet<Datapoint> Datapoints { get; set; }

        public void MarkAsModified(object item)
        {
            Entry(item).State = EntityState.Modified;
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static RegaitContext Create()
        {
            return new RegaitContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>().ToTable("Users").Property(o => o.Id).HasColumnName("UserId");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRoles");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogins");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaims");
            modelBuilder.Entity<IdentityRole>().ToTable("Roles");

            modelBuilder.Entity<ApplicationUser>()
                .HasOptional(o => o.Practitioner)
                .WithRequired(o=> o.User);
            modelBuilder.Entity<ApplicationUser>()
                .HasOptional(o => o.Patient)
                .WithRequired(o => o.User);
            modelBuilder.Entity<ApplicationUser>()
                .Property(o => o.Email)
                .IsRequired();
            modelBuilder.Entity<ApplicationUser>()
                .Property(o => o.UserName)
                .IsRequired();
            modelBuilder.Entity<ApplicationUser>()
                .Property(o => o.PasswordHash)
                .IsRequired();

            modelBuilder.Entity<Hospital>()
                .HasOptional(o => o.Practitioners);
            modelBuilder.Entity<Hospital>()
                .HasOptional(o => o.Patients);
            modelBuilder.Entity<Hospital>().
                Property(o => o.City)
                .IsRequired();
            modelBuilder.Entity<Hospital>().
                Property(o => o.State)
                .IsRequired();
            modelBuilder.Entity<Hospital>().
                Property(o => o.Name)
                .IsRequired();
            modelBuilder.Entity<Hospital>().
                Property(o => o.ZipCode)
                .IsRequired();
            modelBuilder.Entity<Hospital>().
                Property(o => o.PhoneNumber)
                .IsRequired();

            modelBuilder.Entity<Patient>()
                .HasRequired(o => o.Hospital)
                .WithMany(o => o.Patients);
            modelBuilder.Entity<Patient>()
                .HasOptional(o => o.Practitioner)
                .WithMany(o => o.Patients)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Practitioner>()
                .HasRequired(o => o.Hospital)
                .WithMany(o => o.Practitioners);
            modelBuilder.Entity<Practitioner>()
                .Property(o => o.NPI)
                .IsRequired();

            modelBuilder.Entity<Datapoint>()
                .HasRequired(o => o.Session)
                .WithMany(o => o.Datapoints)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<Datapoint>()
                .Property(o => o.BackLeftWeight)
                .IsRequired();
            modelBuilder.Entity<Datapoint>()
                .Property(o => o.BackRightWeight)
                .IsRequired();
            modelBuilder.Entity<Datapoint>()
                .Property(o => o.FrontLeftWeight)
                .IsRequired();
            modelBuilder.Entity<Datapoint>()
                .Property(o => o.FrontRightWeight)
                .IsRequired();
            modelBuilder.Entity<Datapoint>()
                .Property(o => o.LeftHipKneeAngle)
                .IsRequired();
            modelBuilder.Entity<Datapoint>()
                .Property(o => o.RightHipKneeAngle)
                .IsRequired();
            modelBuilder.Entity<Datapoint>()
                .Property(o => o.LeftKneeAnkleAngle)
                .IsRequired();
            modelBuilder.Entity<Datapoint>()
                .Property(o => o.RightKneeAnkleAngle)
                .IsRequired();
            modelBuilder.Entity<Datapoint>()
                .Property(o => o.TimeOfRecording)
                .IsRequired();

            modelBuilder.Entity<Session>()
                .HasRequired(o => o.Patient)
                .WithMany(o => o.Sessions);
            modelBuilder.Entity<Session>()
                .Property(o => o.StartTime)
                .IsRequired();
            modelBuilder.Entity<Session>()
                .Property(o => o.EndTime)
                .IsRequired();
        }
    }
}