using System;
using System.ComponentModel.DataAnnotations;

namespace ScandiumWebsite.Models
{
    using System.Collections.Generic;

    public class Hospital : IdentifiableModel
    {
        public Hospital()
        {
            Practitioners = new List<Practitioner>();
            Patients = new List<Patient>();
        }
        public string Name { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }

        public virtual ICollection<Practitioner> Practitioners { get; set; }
        public virtual ICollection<Patient> Patients { get; set; }
    }
}
