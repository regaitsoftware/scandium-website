using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using ScandiumWebsite.Interfaces;

namespace ScandiumWebsite.Models
{
    public class Person : IPerson
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public abstract class IdentifiableModel
    {
        protected IdentifiableModel()
        {
            Id = Guid.NewGuid().ToString();
        }
        public string Id { get; set; }
    }

    public class Practitioner : IdentifiableModel
    {
        public Practitioner()
        {
            Patients = new List<Patient>();
        }
        public string NPI { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual Hospital Hospital { get; set; }
        public virtual ICollection<Patient> Patients { get; set; }
    }

    public class Patient : IdentifiableModel
    {
        public Patient()
        {
            Sessions = new List<Session>();
        }
        public virtual ApplicationUser User { get; set; }
        public virtual Practitioner Practitioner { get; set; }
        public virtual ICollection<Session> Sessions { get; set; }
        public virtual Hospital Hospital { get; set; }
    }
}