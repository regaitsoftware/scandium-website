﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScandiumWebsite.Models
{
    public class Datapoint: IdentifiableModel
    {
        public DateTime TimeOfRecording { get; set; }
        public double LeftHipKneeAngle { get; set; }
        public double RightHipKneeAngle { get; set; }
        public double LeftKneeAnkleAngle { get; set; }
        public double RightKneeAnkleAngle { get; set; }
        public double FrontLeftWeight { get; set; }
        public double FrontRightWeight { get; set; }
        public double BackLeftWeight { get; set; }
        public double BackRightWeight { get; set; }
        public virtual Session Session { get; set; }
    }
}