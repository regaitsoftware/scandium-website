using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ScandiumWebsite.DataTransferObjects
{
    public class DatapointDto
    {
        [JsonProperty("id")]
        public string Id;

        [JsonProperty("timeOfRecording")]
        [Required]
        public DateTime TimeOfRecording { get; set; }

        [JsonProperty("timeIndex")]
        public TimeSpan TimeIndex { get; set; }

        [JsonProperty("leftHipKneeAngle")]
        [Required]
        public double LeftHipKneeAngle { get; set; }

        [JsonProperty("rightHipKneeAngle")]
        [Required]
        public double RightHipKneeAngle { get; set; }

        [JsonProperty("leftKneeAnkleAngle")]
        [Required]
        public double LeftKneeAnkleAngle { get; set; }

        [JsonProperty("rightKneeAnkleAngle")]
        [Required]
        public double RightKneeAnkleAngle { get; set; }

        [JsonProperty("frontLeftWeight")]
        [Required]
        public double FrontLeftWeight { get; set; }

        [JsonProperty("frontRightWeight")]
        [Required]
        public double FrontRightWeight { get; set; }

        [JsonProperty("backLeftWeight")]
        [Required]
        public double BackLeftWeight { get; set; }

        [JsonProperty("backRightWeight")]
        [Required]
        public double BackRightWeight { get; set; }
    }
}