﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using ScandiumWebsite.Models;

namespace ScandiumWebsite.DataTransferObjects
{
    public class HospitalDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        [Required]
        public string Name { get; set; }

        [JsonProperty("street")]
        [Required]
        public string Street { get; set; }

        [JsonProperty("city")]
        [Required]
        public string City { get; set; }

        [JsonProperty("state")]
        [Required]
        public string State { get; set; }

        [JsonProperty("zipcode")]
        [Required]
        public string ZipCode { get; set; }

        [JsonProperty("phoneNumber")]
        [Required]
        public string PhoneNumber { get; set; }
    }
}