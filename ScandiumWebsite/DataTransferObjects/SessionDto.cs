﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using ScandiumWebsite.Models;
using System.Linq;

namespace ScandiumWebsite.DataTransferObjects
{
    public class SessionDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("patientId")]
        [Required]
        public string PatientId { get; set; }

        [JsonProperty("startTime")]
        [Required]
        public DateTime StartTime { get; set; }

        [JsonProperty("endTime")]
        [Required]
        public DateTime EndTime { get; set; }

        [JsonProperty("datapoints")]
        [Required]
        public virtual IEnumerable<DatapointDto> Datapoints { get; set; }

        public void sortSession()
        {
            Datapoints = Datapoints.OrderBy(d => d.TimeOfRecording);
            DateTime firstPoint = Datapoints.First().TimeOfRecording;
            foreach(DatapointDto dataPoint in Datapoints)
            {
                TimeSpan index = dataPoint.TimeOfRecording.Subtract(firstPoint);
                dataPoint.TimeIndex = index;
            }
        }
    }
}