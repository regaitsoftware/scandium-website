namespace ScandiumWebsite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class optional_first_last_name_user : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Users", name: "PINHash", newName: "PasswordHash");
            AlterColumn("dbo.Users", "FirstName", c => c.String());
            AlterColumn("dbo.Users", "LastName", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "LastName", c => c.String(nullable: false));
            AlterColumn("dbo.Users", "FirstName", c => c.String(nullable: false));
            RenameColumn(table: "dbo.Users", name: "PasswordHash", newName: "PINHash");
        }
    }
}
