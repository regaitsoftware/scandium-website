using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ScandiumWebsite.Models;

namespace ScandiumWebsite.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<RegaitContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(RegaitContext context)
        {
            context.Hospitals.AddOrUpdate(
                h => h.Name,
                new Hospital
                {
                    Name = "Massachusetts General Hospital",
                    Street = "55 Fruit St",
                    City = "Boston",
                    State = "Ma",
                    ZipCode = "02110",
                    PhoneNumber = "(617) 726-2000"
                },
                new Hospital
                {
                    Name = "Shriners Hospitals for Children",
                    Street = "51 Blossom St",
                    City = "Boston",
                    State = "Ma",
                    ZipCode = "02114",
                    PhoneNumber = "(617) 722-3000"
                },
                new Hospital
                {
                    Name = "Spaulding Rehabilitation Hospital Boston",
                    Street = "300 1st Ave",
                    City = "Charlsetown",
                    State = "Ma",
                    ZipCode = "02129",
                    PhoneNumber = "(617) 952-5000"
                },
                new Hospital
                {
                    Name = "Brigham & Women's",
                    Street = "116 Huntington Ave",
                    City = "Boston",
                    State = "Ma",
                    ZipCode = "02116",
                    PhoneNumber = "(617) 732-5500"
                }
            );

            using (var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context)))
            {
                if (!context.Roles.Any(u => u.Name == "Patient"))
                    roleManager.Create(new IdentityRole("Patient"));
                if (!context.Roles.Any(u => u.Name == "Practitioner"))
                    roleManager.Create(new IdentityRole("Practitioner"));
            }

            if (!context.Users.Any(u => u.Email == "testpatient@example.com") && !context.Users.Any(o => o.Email == "testpractitioner@example.com"))
            {
                var patient = new Patient
                {
                    Hospital = context.Hospitals.Single(h => h.Name == "Spaulding Rehabilitation Hospital Boston")
                };

                using (var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context)))
                {
                    var patientUser = new ApplicationUser
                    {
                        UserName = "testpatient@example.com",
                        Email = "testpatient@example.com",
                        FirstName = "Test",
                        LastName = "Patient",
                        Patient = patient
                    };
                    manager.Create(patientUser, "000000");
                    manager.AddToRole(patientUser.Id, "Patient");

                    var practitioner = new Practitioner
                    {
                        Hospital = context.Hospitals.Single(h => h.Name == "Spaulding Rehabilitation Hospital Boston"),
                        Patients = new List<Patient> {patient},
                        NPI = "1111111111"
                    };

                    var practitionerUser = new ApplicationUser
                    {
                        UserName = "testpractitioner@example.com",
                        Email = "testpractitioner@example.com",
                        FirstName = "Test",
                        LastName = "Practitioner",
                        Practitioner = practitioner
                    };

                    manager.Create(practitionerUser, "000000");
                    manager.AddToRole(practitionerUser.Id, "Practitioner");
                }

            }
        }
    }
}
