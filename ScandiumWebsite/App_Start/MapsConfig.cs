﻿using AutoMapper;
using System.Collections.Generic;
using ScandiumWebsite.DataTransferObjects;
using ScandiumWebsite.Models;

namespace ScandiumWebsite
{
    public static class MapsConfig
    {
        public static void Register()
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<Hospital, HospitalDto>();

                config.CreateMap<Session, SessionDto>().ForMember(o => o.PatientId, m => m.MapFrom(x => x.Patient.Id));
//                config.CreateMap<Session, SessionDto>().ForMember(o => o.Datapoints, n => n.Ignore());
                config.CreateMap<SessionDto, Session>().ForMember(o => o.Patient, m => m.Ignore());

                config.CreateMap<Datapoint, DatapointDto>();
                config.CreateMap<DatapointDto, Datapoint>().ForMember(o => o.Session, n => n.Ignore());
            });
        }

        public static T To<T>(this object source)
        {
            return Mapper.Map<T>(source);
        }

        public static IEnumerable<T> To<T>(this IEnumerable<object> source)
        {
            return Mapper.Map<IEnumerable<T>>(source);
        }
    }
}