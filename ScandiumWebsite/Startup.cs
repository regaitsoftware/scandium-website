﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(ScandiumWebsite.Startup))]

namespace ScandiumWebsite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
