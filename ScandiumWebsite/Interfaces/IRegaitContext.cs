﻿using System;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using ScandiumWebsite.Models;

namespace ScandiumWebsite.Interfaces
{
    public interface IRegaitContext : IDisposable
    {
        DbSet<Patient> Patients { get; set; }
        DbSet<Practitioner> Practitioners { get; set; }
        DbSet<Hospital> Hospitals { get; set; }
        DbSet<Session> Sessions { get; set; }
        DbSet<Datapoint> Datapoints { get; set; }
        IDbSet<ApplicationUser> Users { get; set; }
        int SaveChanges();
        void MarkAsModified(object item);
    }
}