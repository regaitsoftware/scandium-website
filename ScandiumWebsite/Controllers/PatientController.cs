﻿using System.Threading.Tasks;
using System.Web.Mvc;
using ScandiumWebsite.Models;

namespace ScandiumWebsite.Controllers
{
    [Authorize(Roles = "Patient")]
    public class PatientController : AccountController
    {
        //
        // GET: /Patient/Home
        public ActionResult Home()
        {
            return View();
        }

        //
        // GET: /Patient/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToLocal("Home");
            ViewBag.HospitalList = GetHospitals();
            ViewBag.PractitionerList = GetPractitioners();
            return View();
        }

        //
        // POST: /Patient/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(PatientRegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var practitioner = await Context.Practitioners.FindAsync(model.PractitionerId);
                if (practitioner == null)
                {
                    ModelState.AddModelError("", "No Practitioner found.");
                }
                var hospital = await Context.Hospitals.FindAsync(model.HospitalId);
                if (hospital == null)
                {
                    ModelState.AddModelError("", "No Hospital found.");
                }
                var patient = new Patient
                {
                    Hospital = hospital,
                    Practitioner = practitioner
                };
                var user = new ApplicationUser
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    UserName = model.Email,
                    Email = model.Email,
                    Patient = patient,
                };

                var result = await UserManager.CreateAsync(user, model.PIN);
                if (result.Succeeded)
                {
                    await UserManager.AddToRoleAsync(user.Id, "Patient");
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return RedirectToAction("Home", "Patient");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
    }
}
