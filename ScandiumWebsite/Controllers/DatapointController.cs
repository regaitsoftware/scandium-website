﻿using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity.Owin;
using ScandiumWebsite.DataTransferObjects;
using ScandiumWebsite.Interfaces;
using ScandiumWebsite.Models;

namespace ScandiumWebsite.Controllers
{
    [System.Web.Mvc.Authorize]
    public class DatapointController : ApiController
    {
        private IRegaitContext _context;

        public DatapointController() {}

        public DatapointController(IRegaitContext context)
        {
            Context = context;
        }

        public IRegaitContext Context
        {
            get
            {
                return _context ?? HttpContext.Current.GetOwinContext().Get<RegaitContext>();
            }
            private set { _context = value; }
        }

        // GET: api/Datapoint
        [ResponseType(typeof(IDbAsyncEnumerable<DatapointDto>))]
        public IHttpActionResult Get()
        {
            var response = Context.Datapoints.To<DatapointDto>();
            return Ok(response);
        }

        // GET: api/Datapoint/5
        [ResponseType(typeof(DatapointDto))]
        public IHttpActionResult Get(string id)
        {
            Datapoint datapoint = Context.Datapoints.Find(id);
            if (datapoint == null)
            {
                return NotFound();
            }
            var response = datapoint.To<DatapointDto>();
            return Ok(response);
        }

        // PUT: api/Datapoint/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Put(string id, [FromBody] Datapoint datapoint)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != datapoint.Id)
            {
                return BadRequest();
            }

            if (!DatapointExists(id))
            {
                return NotFound();
            }

            var datapointToUpdate = datapoint;
            Context.MarkAsModified(datapointToUpdate);
            Context.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Hospitals
        [ResponseType(typeof(HospitalDto))]
        public IHttpActionResult Post([FromBody] Hospital hospital)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Context.Hospitals.Add(hospital);

            try
            {
                Context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (DatapointExists(hospital.Id))
                {
                    return Conflict();
                }
                throw;
            }

            return CreatedAtRoute("DefaultApi", new { id = hospital.Id }, hospital.To<HospitalDto>());
        }

        // DELETE: api/Datapoint/5
        [ResponseType(typeof(DatapointDto))]
        public IHttpActionResult Delete(string id)
        {
            Datapoint datapoint = Context.Datapoints.Find(id);
            if (datapoint == null)
            {
                return NotFound();
            }

            Context.Datapoints.Remove(datapoint);
            Context.SaveChanges();

            return Ok(datapoint.To<DatapointDto>());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DatapointExists(string id)
        {
            return Context.Datapoints.Count(e => e.Id == id) > 0;
        }
    }
}