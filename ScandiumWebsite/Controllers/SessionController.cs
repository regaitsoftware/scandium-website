﻿using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using ScandiumWebsite.DataTransferObjects;
using ScandiumWebsite.Interfaces;
using ScandiumWebsite.Models;
using Newtonsoft.Json;

namespace ScandiumWebsite.Controllers
{
    [System.Web.Mvc.Authorize]
    public class SessionController : ApiController
    {
        private IRegaitContext _context;

        public SessionController() {}

        public SessionController(IRegaitContext context)
        {
            Context = context;
        }

        public IRegaitContext Context
        {
            get
            {
                return _context ?? HttpContext.Current.GetOwinContext().Get<RegaitContext>();
            }
            private set { _context = value; }
        }


        // GET: api/Sessions
        [ResponseType(typeof(IDbAsyncEnumerable<SessionDto>))]
        public IHttpActionResult Get()
        {
            var response = Context.Sessions.To<SessionDto>();
            return Ok(response);
        }

        // GET: api/Sessions/5
        [ResponseType(typeof(SessionDto))]
        public IHttpActionResult Get(string id)
        {
            Session session = Context.Sessions.Find(id);
            if (session == null)
            {
                return NotFound();
            }
            var response = session.To<SessionDto>();
            response.sortSession();
            string json = JsonConvert.SerializeObject(response);
            return Ok(response);
        }

        // PUT: api/Sessions/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Put(string id, [FromBody] Session session)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != session.Id)
            {
                return BadRequest();
            }

            if (!SessionExists(id))
            {
                return NotFound();
            }
            var sessionToUpdate = session;
            Context.MarkAsModified(sessionToUpdate);
            Context.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Sessions
        [ResponseType(typeof(SessionDto))]
        public IHttpActionResult Post([FromBody] SessionDto session)
        {
            var patient = Context.Patients.Find(session.PatientId);
            var convertedSession = session.To<Session>();
            convertedSession.Patient = patient;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Context.Sessions.Add(convertedSession);

            try
            {
                Context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (SessionExists(convertedSession.Id))
                {
                    return Conflict();
                }
                throw;
            }

            return CreatedAtRoute("DefaultApi", new { id = session.Id }, convertedSession.To<SessionDto>());
        }

        // DELETE: api/Sessions/5
        [ResponseType(typeof(SessionDto))]
        public IHttpActionResult Delete(string id)
        {
            Session session = Context.Sessions.Find(id);
            if (session == null)
            {
                return NotFound();
            }

            Context.Sessions.Remove(session);
            Context.SaveChanges();

            return Ok(session.To<SessionDto>());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SessionExists(string id)
        {
            return Context.Sessions.Count(e => e.Id == id) > 0;
        }
    }
}