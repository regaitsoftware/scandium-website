﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ScandiumWebsite.Models;

namespace ScandiumWebsite.Controllers
{
    [Authorize(Roles = "Practitioner")]
    public class PractitionerController : AccountController
    {
        //
        // GET: /Practitioner/Home
        [HttpGet]
        public ActionResult Home()
        {
            ViewBag.PatientList = GetPractitionerPatients(User.Identity.GetUserId());
            return View();
        }

        private List<SelectListItem> GetPractitionerPatients(string id)
        {
            var patients = Context.Practitioners.Find(id).Patients;
            return new List<SelectListItem>(from patient in patients
                                            select new SelectListItem
                                            {
                                                Text = $"{patient.User.FirstName} {patient.User.LastName}",
                                                Value = patient.Id
                                            });
        }

        private List<SelectListItem> GetUnassignedPatients()
        {
            var query = (from patient in Context.Patients
                        where patient.Practitioner == null
                        select patient).AsEnumerable();
            return new List<SelectListItem>(query.Select(patient => new SelectListItem
            {
                Text = $"{patient.User.FirstName} {patient.User.LastName}",
                Value = patient.Id
            }));
        }

        private List<SelectListItem> GetPatientSessions(string id)
        {
            var sessions = (from session in Context.Sessions where session.Patient.Id == id select session);
            //var patients = Context.Practitioners.Find(id).Patients;
            return new List<SelectListItem>(from session in sessions select new SelectListItem
                                            {
                                                Text = session.StartTime.ToString(),
                                                Value = session.Id
                                            });
        }

        //
        // Post: /Practitioner/RequestPatientChart
        [HttpPost]
        public ActionResult RequestPatientChart(RequestPatientChartViewModel model)
        {
            if (!ModelState.IsValid) return null;
            ViewBag.SessionList = GetPatientSessions(model.PatientId);
            //if (model.StartDate != null && model.EndDate != null)
            //{
            //    sessions = (from session in sessions
            //                where session.StartTime == model.StartDate.Value &&
            //                      session.EndTime == model.EndDate.Value
            //                select session);
            //}
            //ViewBag.sessions - sessions
            return View();//return RedirectToLocal("Home");
        }

        //
        // POST: /Practitioner/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(PractitionerRegisterViewModel model)
        {
            if (!ModelState.IsValid) return View(model);
            List<Patient> tmpPatients = new List<Patient>();
            foreach (var id in model.PatientIds ?? new List<string>())
            {
                var tmp = await Context.Patients.FindAsync(id);
                if (tmp == null)
                {
                    ModelState.AddModelError("", "Patient not found.");
                }
                else
                {
                    tmpPatients.Add(tmp);
                }
            }

            var hospital = await Context.Hospitals.FindAsync(model.HospitalId);
            if (hospital == null)
            {
                ModelState.AddModelError("", "Hospital not found.");
            }

            var practitioner = new Practitioner
            {
                NPI = model.NPI,
                Hospital = hospital,
                Patients = tmpPatients
            };
            var user = new ApplicationUser
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                UserName = model.Email,
                Email = model.Email,
                Practitioner = practitioner

            };

            var result = await UserManager.CreateAsync(user, model.PIN);
            if (result.Succeeded)
            {
                var roleResult = await UserManager.AddToRoleAsync(user.Id, "Practitioner");
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                return RedirectToAction("Home", "Practitioner");
            }
            AddErrors(result);

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Practitioner/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToLocal("Home");
            ViewBag.HospitalList = GetHospitals();
            ViewBag.PatientList = GetUnassignedPatients();
            return View();
        }
    }
}
