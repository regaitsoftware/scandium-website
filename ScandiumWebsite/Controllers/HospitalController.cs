﻿using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity.Owin;
using ScandiumWebsite.DataTransferObjects;
using ScandiumWebsite.Interfaces;
using ScandiumWebsite.Models;

namespace ScandiumWebsite.Controllers
{
    [Authorize]
    public class HospitalController : ApiController
    {
        private IRegaitContext _context;

        public HospitalController() {}

        public HospitalController(IRegaitContext context)
        {
            Context = context;
        }

        public IRegaitContext Context
        {
            get
            {
                return _context ?? HttpContext.Current.GetOwinContext().Get<RegaitContext>();
            }
            private set { _context = value; }
        }

        // GET: api/Hospital
        [AllowAnonymous]
        [ResponseType(typeof(IDbAsyncEnumerable<HospitalDto>))]
        public IHttpActionResult Get()
        {
            var response = Context.Hospitals.To<HospitalDto>();
            return Ok(response);
        }

        // GET: api/Hospital/5
        [AllowAnonymous]
        [ResponseType(typeof(HospitalDto))]
        public IHttpActionResult Get(string id)
        {
            Hospital hospital = Context.Hospitals.Find(id);
            if (hospital == null)
            {
                return NotFound();
            }
            var response = hospital.To<HospitalDto>();
            return Ok(response);
        }

        // PUT: api/Hospital/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Put(string id, [FromBody] Hospital hospital)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != hospital.Id)
            {
                return BadRequest();
            }

            if (!HospitalExists(id))
            {
                return NotFound();
            }

            var hospitalToUpdate = hospital;
            Context.MarkAsModified(hospitalToUpdate);
            Context.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Hospital/
        [ResponseType(typeof(HospitalDto))]
        public IHttpActionResult Post([FromBody] Hospital hospital)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Context.Hospitals.Add(hospital);

            try
            {
                Context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (HospitalExists(hospital.Id))
                {
                    return Conflict();
                }
                throw;
            }

            return CreatedAtRoute("DefaultApi", new { id = hospital.Id }, hospital.To<HospitalDto>());
        }

        // DELETE: api/Hospital/5
        [ResponseType(typeof(HospitalDto))]
        public IHttpActionResult Delete(string id)
        {
            Hospital hospital = Context.Hospitals.Find(id);
            if (hospital == null)
            {
                return NotFound();
            }

            Context.Hospitals.Remove(hospital);
            Context.SaveChanges();

            return Ok(hospital.To<HospitalDto>());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HospitalExists(string id)
        {
            return Context.Hospitals.Count(e => e.Id == id) > 0;
        }
    }
}