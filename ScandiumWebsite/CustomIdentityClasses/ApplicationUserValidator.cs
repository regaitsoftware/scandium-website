﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using ScandiumWebsite.Models;

namespace ScandiumWebsite.CustomIdentityClasses
{
    public class ApplicationUserValidator : IIdentityValidator<ApplicationUser>
    {
        public bool AllowOnlyAlphanumericUserNames = false;
        public bool RequireUniqueEmail = true;
        private ApplicationUserManager _userManager;

        public ApplicationUserValidator(ApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public Task<IdentityResult> ValidateAsync(ApplicationUser item)
        {
            if (RequireUniqueEmail) {
                var owner = UserManager.FindByEmail(item.Email);
                if (owner != null && !string.Equals(item.Id, owner.Id))
                {
                    return Task.FromResult(IdentityResult.Failed("Email is not unique"));
                }
            }

            if (AllowOnlyAlphanumericUserNames && !item.Email.Any(IsLetter))
            {
                return Task.FromResult(IdentityResult.Failed("Email must contain only letters"));
            }

            return Task.FromResult(IdentityResult.Success);
        }

        public virtual bool IsLower(char c)
        {
            return c >= 'a' && c <= 'z';
        }

        public virtual bool IsUpper(char c)
        {
            return c >= 'A' && c <= 'Z';
        }

        public virtual bool IsLetter(char c)
        {
            return IsLower(c) && IsUpper(c);
        }
    }
}