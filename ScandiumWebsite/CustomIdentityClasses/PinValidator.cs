﻿// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace ScandiumWebsite.CustomIdentityClasses
{
    public class PINValidator : IIdentityValidator<string>
    {
        public int PINLength { get; set; }

        public Task<IdentityResult> ValidateAsync(string item)
        {
            if (string.IsNullOrEmpty(item) || item.Length != PINLength)
            {
                return Task.FromResult(IdentityResult.Failed($"PIN Length Required is:{PINLength}"));
            }

            if (item.Any(char.IsLetter))
            {
                return Task.FromResult(IdentityResult.Failed("The PIN must not contain letters"));
            }

            return Task.FromResult(IdentityResult.Success);
        }
    }
}