﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScandiumWebsite.Controllers;
using ScandiumWebsite.DataTransferObjects;
using ScandiumWebsite.Models;

namespace ScandiumWebsite.Tests.Controllers
{
    [TestClass()]
    public class HospitalsControllerTests
    {
        private TestRegaitContext _context;
        private HospitalController _controller;

        private Hospital _testHospital1,
                         _testHospital2,
                         _testHospital3;

        [TestInitialize]
        public void SetUp()
        {
            _testHospital1 = new Hospital
            {

                Name = "Demo1",
                Street = "test street",
                State = "State",
                City = "Demo1",
                ZipCode = "02145",
                PhoneNumber = "111-111-1111"
            };
            _testHospital2 = new Hospital
            {
                Name = "Demo2",
                Street = "test street",
                State = "State",
                City = "Demo2",
                ZipCode = "02145",
                PhoneNumber = "111-222-3333"
            };
            _testHospital3 = new Hospital
            {
                Name = "Demo3",
                Street = "test street",
                State = "State",
                City = "Demo3",
                ZipCode = "02145",
                PhoneNumber = "111-333-3333"

            };
            _context = new TestRegaitContext();
            _controller = new HospitalController(_context);
        }

        [TestCleanup]
        public void TearDown()
        {
            _context.Dispose();
        }

        [TestMethod()]
        public void Get_All_Should_Return_All_Hospitals()
        {
            _context.Hospitals.Add(_testHospital1);
            _context.Hospitals.Add(_testHospital2);
            _context.Hospitals.Add(_testHospital3);
            var results = _controller.Get() as OkNegotiatedContentResult<IEnumerable<HospitalDto>>;
            Assert.IsNotNull(results);
            Assert.AreEqual(3, results.Content.Count());
        }

        [TestMethod()]
        public void Get_All_Should_Return_No_Hospitals_When_Non_Exist()
        {
            var results = _controller.Get() as OkNegotiatedContentResult<IEnumerable<HospitalDto>>;
            Assert.IsNotNull(results);
            Assert.AreEqual(0, results.Content.Count());
        }

        [TestMethod()]
        public void Get_Hospital_By_Id_Should_Return_That_Hospital()
        {
            _context.Hospitals.Add(_testHospital1);
            _context.Hospitals.Add(_testHospital2);
            var results = _controller.Get(_testHospital1.Id) as OkNegotiatedContentResult<HospitalDto>;
            Assert.IsNotNull(results);
            Assert.AreEqual(_testHospital1.Name, results.Content.Name);
        }

        [TestMethod()]
        public void Get_Hospitals_By_Id_That_Does_Not_Exist_Should_Return_No_Hospital()
        {
            var results = _controller.Get("1");
            Assert.IsInstanceOfType(results, typeof(NotFoundResult));
        }

        [TestMethod()]
        public void Delete_Nonexistant_Hospital_Should_Return_NotFound()
        {
            var results = _controller.Delete("1");
            Assert.IsInstanceOfType(results, typeof(NotFoundResult));
        }

        [TestMethod()]
        public void Delete_Existant_Hospital_Should_Return_A_Deleted_Hospital()
        {
            _context.Hospitals.Add(_testHospital1);
            var results = _controller.Delete(_testHospital1.Id) as OkNegotiatedContentResult<HospitalDto>;
            Assert.IsNotNull(results);
            Assert.AreEqual(_testHospital1.Name, results.Content.Name);
            Assert.IsNull(_context.Hospitals.Find(_testHospital1.Id));
        }

        [TestMethod()]
        public void Post_Nonexistant_Hospital_Should_Return_A_Good_Response()
        {
            var results = _controller.Post(_testHospital1) as CreatedAtRouteNegotiatedContentResult<HospitalDto>;
            Assert.IsNotNull(results);
            Assert.AreEqual(results.Content.Name, _testHospital1.Name);
        }

        [TestMethod()]
        public void Post_Existant_Hospital_Should_Fail()
        {
            _controller.Post(_testHospital1);
            var results = _controller.Post(_testHospital1);
            Assert.IsInstanceOfType(results, typeof(ConflictResult));
        }

        [TestMethod()]
        public void Put_Existant_Hospital_Should_Update_Hospital()
        {
            _controller.Post(_testHospital1);
            _testHospital1.Name = "A new place";
            var results = _controller.Put(_testHospital1.Id, _testHospital1) as StatusCodeResult;
            Assert.IsNotNull(results);
            Assert.AreEqual(HttpStatusCode.NoContent, results.StatusCode);
            Assert.AreEqual(_testHospital1.Name, _context.Hospitals.Find(_testHospital1.Id).Name);
        }

        [TestMethod()]
        public void Put_NonExistant_Hospital_Should_Fail()
        {
            var results = _controller.Put(_testHospital1.Id, _testHospital1);
            Assert.IsInstanceOfType(results, typeof(NotFoundResult));
        }
    }
}