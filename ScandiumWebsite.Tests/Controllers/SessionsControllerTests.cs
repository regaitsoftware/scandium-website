﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScandiumWebsite.Controllers;
using ScandiumWebsite.DataTransferObjects;
using ScandiumWebsite.Models;

namespace ScandiumWebsite.Tests.Controllers
{
    [TestClass()]
    public class SessionsControllerTests
    {
        private TestRegaitContext _context;
        private SessionController _controller;
        private Session _testSession1,
                        _testSession2,
                        _testSession3;

        private List<Datapoint> _testDatapoints;
        private Patient _testPatient;

        [TestInitialize]
        public void SetUp()
        {
            _testDatapoints = new List<Datapoint>();
            _testPatient = new Patient()
            {
                Hospital =
                    new Hospital
                    {
                        City = "A",
                        State = "B",
                        Name = "C",
                        PhoneNumber = "22112233",
                        Street = "Z",
                        ZipCode = "02121"
                    }
            };
            for (int i = 1; i < 10; i++)
            {
                var tmp = new Datapoint
                {
                    BackLeftWeight = i,
                    BackRightWeight = i,
                    FrontLeftWeight = i,
                    FrontRightWeight = i,
                    LeftHipKneeAngle = i,
                    RightKneeAnkleAngle = i,
                    TimeOfRecording = new DateTime(2016, i, i, i, i, i, i),
                };
                _testDatapoints.Add(tmp);
            }

            _testSession1 = new Session
            {
                Patient = _testPatient,
                Datapoints = _testDatapoints,
                StartTime = new DateTime(2016, 1, 1, 12, 0, 0),
                EndTime = new DateTime(2016, 1, 1, 12, 30, 0),
                
            };
            _testSession2 = new Session
            {
                Patient = _testPatient,
                Datapoints = _testDatapoints,
                StartTime = new DateTime(2016, 1, 1, 12, 0, 0),
                EndTime = new DateTime(2016, 1, 1, 12, 30, 0)
            };
            _testSession3 = new Session
            {
                Patient = _testPatient,
                Datapoints = _testDatapoints,
                StartTime = new DateTime(2016, 1, 1, 12, 0, 0),
                EndTime = new DateTime(2016, 1, 1, 12, 30, 0)
            };
            _context = new TestRegaitContext();
            _controller = new SessionController(_context);
        }

        [TestCleanup]
        public void TearDown()
        {
            _context.Dispose();
        }

        [TestMethod()]
        public void Get_All_Should_Return_All_Sessions()
        {
            _context.Sessions.Add(_testSession1);
            _context.Sessions.Add(_testSession2);
            _context.Sessions.Add(_testSession3);
            var results = _controller.Get() as OkNegotiatedContentResult<IEnumerable<SessionDto>>;
            Assert.IsNotNull(results);
            Assert.AreEqual(3, results.Content.Count());
        }

        [TestMethod()]
        public void Get_All_Should_Return_No_Session_When_Non_Exist()
        {
            var results = _controller.Get() as OkNegotiatedContentResult<IEnumerable<SessionDto>>;
            Assert.IsNotNull(results);
            Assert.AreEqual(0, results.Content.Count());
        }

        [TestMethod()]
        public void Get_Sessions_By_Id_Should_Return_That_Session()
        {
            _context.Sessions.Add(_testSession1.To<Session>());
            _context.Sessions.Add(_testSession2.To<Session>());
            var results = _controller.Get(_testSession1.Id) as OkNegotiatedContentResult<SessionDto>;
            Assert.IsNotNull(results);
            Assert.AreEqual(_testSession1.Id, results.Content.Id);
        }

        [TestMethod()]
        public void Get_Session_By_Id_That_Does_Not_Exist_Should_Return_No_Session()
        {
            var results = _controller.Get("1");
            Assert.IsInstanceOfType(results, typeof(NotFoundResult));
        }

        [TestMethod()]
        public void Delete_Nonexistant_Session_Should_Return_NotFound()
        {
            var results = _controller.Delete("1");
            Assert.IsInstanceOfType(results, typeof(NotFoundResult));
        }

        [TestMethod()]
        public void Delete_Existant_Sessions_Should_Return_A_Deleted_Session()
        {
            _context.Sessions.Add(_testSession1);
            var results = _controller.Delete(_testSession1.Id) as OkNegotiatedContentResult<SessionDto>;
            Assert.IsNotNull(results);
            Assert.AreEqual(_testSession1.Id, results.Content.Id);
            Assert.IsNull(_context.Sessions.Find(results.Content.Id));
        }

        [TestMethod()]
        public void Post_Nonexistant_Session_Should_Return_A_Good_Response()
        {
            var results = _controller.Post(_testSession1.To<SessionDto>()) as CreatedAtRouteNegotiatedContentResult<SessionDto>;
            Assert.IsNotNull(results);
        }

        [TestMethod()]
        public void Post_Existant_Session_Should_Fail()
        {
            _controller.Post(_testSession1.To<SessionDto>());
            var results = _controller.Post(_testSession1.To<SessionDto>());
            Assert.IsInstanceOfType(results, typeof(ConflictResult));
        }

        [TestMethod()]
        public void Put_Existant_Session_Should_Update_Sessions()
        {
            _controller.Post(_testSession1.To<SessionDto>());
            var results = _controller.Put(_testSession1.Id, _testSession1) as StatusCodeResult;
            Assert.IsNotNull(results);
            Assert.AreEqual(HttpStatusCode.NoContent, results.StatusCode);
        }

        [TestMethod()]
        public void Put_NonExistant_Session_Should_Fail()
        {
            var results = _controller.Put(_testSession1.Id, _testSession1);
            Assert.IsInstanceOfType(results, typeof(NotFoundResult));
        }
    }
}