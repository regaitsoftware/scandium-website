﻿using System.Linq;
using ScandiumWebsite.Models;

namespace ScandiumWebsite.Tests
{
    internal class TestApplicationUserDbSet : TestDbSet<ApplicationUser>
    {
        public override ApplicationUser Find(params object[] keyValues)
        {
            return this.SingleOrDefault(user => user.Id == (string)keyValues.Single());
        }
    }

    internal class TestHospitalDbSet : TestDbSet<Hospital>
    {
        public override Hospital Find(params object[] keyValues)
        {
            return this.SingleOrDefault(patient => patient.Id == (string)keyValues.Single());
        }
    }

    internal class TestDatapointDbSet : TestDbSet<Datapoint>
    {
        public override Datapoint Find(params object[] keyValues)
        {
            return this.SingleOrDefault(datapoint => datapoint.Id == (string)keyValues.Single());
        }
    }

    internal class TestSessionDbSet : TestDbSet<Session>
    {
        public override Session Find(params object[] keyValues)
        {
            return this.SingleOrDefault(session => session.Id == (string)keyValues.Single());
        }
    }

    internal class TestPatientDbSet : TestDbSet<Patient>
    {
        public override Patient Find(params object[] keyValues)
        {
            return this.SingleOrDefault(patient => patient.Id == (string) keyValues.Single());
        }
    }

    internal class TestPractitionerDbSet : TestDbSet<Practitioner>
    {
        public override Practitioner Find(params object[] keyValues)
        {
            return this.SingleOrDefault(practitioner => practitioner.Id == (string)keyValues.Single());
        }
    }
}