﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using ScandiumWebsite.Interfaces;
using ScandiumWebsite.Models;

namespace ScandiumWebsite.Tests
{
    public class TestRegaitContext : IRegaitContext
    {
        public TestRegaitContext()
        {
            MapsConfig.Register();
            this.Users = new TestApplicationUserDbSet();
            this.Hospitals = new TestHospitalDbSet();
            this.Datapoints = new TestDatapointDbSet();
            this.Sessions = new TestSessionDbSet();
            this.Patients = new TestPatientDbSet();
            this.Practitioners = new TestPractitionerDbSet();
        }

        public DbSet<Patient> Patients { get; set; }
        public DbSet<Practitioner> Practitioners { get; set; }
        public DbSet<Hospital> Hospitals { get; set; }
        public IDbSet<ApplicationUser> Users { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Datapoint> Datapoints { get; set; }

        public int SaveChanges()
        {
            if (Users.GroupBy(x => x.Id).Count(x => x.Count() > 1) > 0 ||
                Hospitals.GroupBy(x => x.Id).Count(x => x.Count() > 1) > 0 ||
                Sessions.GroupBy(x => x.Id).Count(x => x.Count() > 1) > 0 ||
                Practitioners.GroupBy(x => x.Id).Count(x => x.Count() > 1) > 0 ||
                Patients.GroupBy(x => x.Id).Count(x => x.Count() > 1) > 0 ||
                Datapoints.GroupBy(x => x.Id).Count(x => x.Count() > 1) > 0)
                throw new DbUpdateException();
            return 0;
        }

        public void MarkAsModified(object item) { }

        public void Dispose()
        {
            Users = new TestDbSet<ApplicationUser>();
            Hospitals = new TestDbSet<Hospital>();
            Sessions = new TestDbSet<Session>();
        }
    }
}